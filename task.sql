CREATE INDEX birth_date ON user (birth_date);

CREATE INDEX user_id ON phone_number (user_id);

SELECT u.name, count(p.id)
FROM user as u
       LEFT JOIN phone_number as p ON u.id = p.user_id
WHERE YEAR(u.birth_date) <= YEAR(CURDATE()) - 28
  AND YEAR(u.birth_date) >= YEAR(CURDATE()) - 38
;